package com.example.PhotoAlbum28.PhotoAlbum28;

/**
 *  Created by Mohammad Memon and Jiya Kohli
 */

public class AppConstants {

    public static final String MODE = "mode";
    public static final String ALBUM_TITLE = "album_title";
    public static final String PHOTO_FILENAME = "photo_filename";
    public static final String PHOTO_FILEPATH = "photo_filePath";
    public static final String URI = "uri";
    public static final String QUERY = "query";


}
